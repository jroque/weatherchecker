package jeremiahroque.com.weatherchecker.worker;

import android.net.Uri;
import android.os.AsyncTask;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import jeremiahroque.com.weatherchecker.model.DailyWeatherForecast;
import jeremiahroque.com.weatherchecker.model.WeatherInfo;

/**
 * AsyncTask to grab the data from worldweatheronline.com
 */
public class WeatherFetcher extends AsyncTask<String, Integer, Map<String, Object>> {

    // https://api.worldweatheronline.com/free/v2/weather.ashx
    // https://api.worldweatheronline.com/premium/v1/weather.ashx
    private static final String PROTOCOL = "https";
    private static final String HOST = "api.worldweatheronline.com";
    private static final String WWO_ACCESS = "premium";
    private static final String WWO_VERSION = "v1";
    private static final String WWO_FILE = "weather.ashx";
    private static final String LOCATION_INPUT = "q";
    private static final String API_KEY = "key";
    private static final String NUM_DAYS_INPUT = "num_of_days";
    private static final String FORMAT_KEY = "format";

    public static final String KEY_FORECAST = "weather";
    public static final String KEY_CURRENT_CONDITION = "current_condition";

    private JSONWeatherAdapter jsonWeatherAdapter;
    private String apiKey;
    private WeatherFetcherListener weatherFetcherListener;


    public WeatherFetcher(WeatherFetcherListener weatherFetcherListener, String apiKey) {
        this.jsonWeatherAdapter = new JSONWeatherAdapter();
        this.weatherFetcherListener = weatherFetcherListener;
        this.apiKey = apiKey;
    }

    @Override
    protected Map<String, Object> doInBackground(String... strings) {
        String location = strings[0];
        String JSON = getWeatherJSON(location, 6); // Retrieve today + next five days
        Map<String, Object> resultData = new HashMap<>();

        try {

            JSONObject jObject = new JSONObject(JSON);
            JSONObject data = jObject.getJSONObject("data");

            JSONArray fiveDayForecastJSON = data.getJSONArray(KEY_FORECAST);
            ArrayList<DailyWeatherForecast> futureForecast = new ArrayList<>();
            // Skip the first one, as it's the current day.
            for (int i = 1; i < fiveDayForecastJSON.length(); ++i) {
                JSONObject futureWeatherJSON = fiveDayForecastJSON.getJSONObject(i);
                DailyWeatherForecast dailyWeatherForecast = jsonWeatherAdapter.parseFutureWeather(futureWeatherJSON, location);
                futureForecast.add(dailyWeatherForecast);
            }

            JSONObject current = (JSONObject) data.getJSONArray(KEY_CURRENT_CONDITION).get(0);
            WeatherInfo weatherInfo = jsonWeatherAdapter.parseCurentConditions(current, location);

            SimpleDateFormat sdf = new SimpleDateFormat("EE, MMMM d");
            weatherInfo.setDate(sdf.format(new Date()));

            resultData.put(KEY_CURRENT_CONDITION, weatherInfo);
            resultData.put(KEY_FORECAST, futureForecast);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return resultData;
    }

    /**
     * Makes the call to WorldWeatherOnline & returns the JSON.
     * @param location
     * @param numDays
     * @return
     */
    private String getWeatherJSON(String location, int numDays) {
        Uri.Builder builder = new Uri.Builder();
        builder.scheme(PROTOCOL)
               .authority(HOST)
               .appendPath(WWO_ACCESS)
               .appendPath(WWO_VERSION)
               .appendPath(WWO_FILE)
               .appendQueryParameter(API_KEY, apiKey)
               .appendQueryParameter(LOCATION_INPUT, location)
               .appendQueryParameter(NUM_DAYS_INPUT, Integer.toString(numDays))
               .appendQueryParameter(FORMAT_KEY, "json");

        String result = "";
        InputStream is = null;

        try {
            URL url = new URL(builder.build().toString());
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(10000);
            conn.setConnectTimeout(15000);
            conn.setRequestMethod("GET");
            conn.setDoInput(true);
            conn.connect();

            is = conn.getInputStream();
            result = readInputStream(is);

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (is != null) {
                try {
                    is.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return result;
    }

    protected void onPostExecute(Map<String, Object> result) {
        weatherFetcherListener.callback(result);
    }

    private String readInputStream(InputStream stream) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(stream, "UTF-8"));
        StringBuilder sb = new StringBuilder();
        String line;
        while ((line = reader.readLine()) != null) {
            sb.append(line);
        }
        return sb.toString();
    }

}
