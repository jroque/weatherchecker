package jeremiahroque.com.weatherchecker.worker;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import jeremiahroque.com.weatherchecker.model.DailyWeatherForecast;
import jeremiahroque.com.weatherchecker.model.WeatherInfo;

/**
 * Converts the data retrieved from worldweatheronline.com from JSON to our Java classes.
 */
public class JSONWeatherAdapter {

    public static final String KEY_DATE = "date";
    public static final String KEY_DESC = "weatherDesc";
    public static final String KEY_HOURLY = "hourly";
    public static final String KEY_ICON_URL = "weatherIconUrl";
    public static final String KEY_MAX_TEMP_F = "maxtempF";
    public static final String KEY_MIN_TEMP_F = "mintempF";
    public static final String KEY_PRECIP_IN = "precipInches";
    public static final String KEY_PRECIP_MM = "precipMM";
    public static final String KEY_TEMP_F = "temp_F";
    public static final String KEY_VALUE = "value";


    public WeatherInfo parseCurentConditions(JSONObject jsonObject, String location) throws JSONException {
        WeatherInfo weatherInfo = new WeatherInfo();

        weatherInfo.setLocation(location);
        weatherInfo.setCurrentFahrenheit(jsonObject.optInt(KEY_TEMP_F));

        // precipInches isn't always there, despite what the API claims.  :(
        String precipitationString =  jsonObject.optString(KEY_PRECIP_IN);
        float precipitation;
        if (precipitationString.equals("")) {
            float precipitationMM = Float.parseFloat(jsonObject.optString(KEY_PRECIP_MM));
            precipitation = precipitationMM * .03937f;
        } else {
            precipitation = Float.parseFloat(precipitationString);
        }
        weatherInfo.setPrecipitationInches(precipitation);

        JSONArray weatherDesc = jsonObject.optJSONArray(KEY_DESC);
        if (weatherDesc != null) {
            weatherInfo.setWeatherDescription(weatherDesc.optJSONObject(0).optString(KEY_VALUE));
        }

        JSONArray weatherIconURL = jsonObject.optJSONArray(KEY_ICON_URL);
        if (weatherIconURL != null) {
            weatherInfo.setWeatherIconURL(weatherIconURL.optJSONObject(0).optString(KEY_VALUE));
        }
        return weatherInfo;
    }

    public DailyWeatherForecast parseFutureWeather(JSONObject jsonObject, String location) throws JSONException {
        DailyWeatherForecast weather = new DailyWeatherForecast();

        weather.setMinTempFahrenheit(jsonObject.optInt(KEY_MIN_TEMP_F));
        weather.setMaxTempFahrenheit(jsonObject.optInt(KEY_MAX_TEMP_F));

        String date = jsonObject.optString(KEY_DATE);
        if (date != null) {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yy-MM-dd");
            try {
                Date dateObj = dateFormat.parse(date);
                SimpleDateFormat sdf = new SimpleDateFormat("EE, MMMM d");
                String dateStr = sdf.format(dateObj);
                weather.setDate(dateStr);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        ArrayList<WeatherInfo> weatherInfoList = new ArrayList<WeatherInfo>();
        JSONArray hourlyData = jsonObject.optJSONArray(KEY_HOURLY);
        for (int i = 0; i < hourlyData.length(); ++i) {
            WeatherInfo weatherInfo = parseCurentConditions(hourlyData.getJSONObject(i), location);
            weatherInfoList.add(weatherInfo);
        }
        weather.setHourlyWeatherForecastList(weatherInfoList);

        return weather;
    }


}
