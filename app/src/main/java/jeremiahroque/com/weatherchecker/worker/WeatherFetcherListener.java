package jeremiahroque.com.weatherchecker.worker;

import java.util.Map;

/**
 * This interface works in conjunction with WeatherFetcher, so that something is done with the data
 * received.
 */
public interface WeatherFetcherListener {
    public void callback(Map<String, Object> weatherResult);
}
