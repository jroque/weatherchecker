package jeremiahroque.com.weatherchecker;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;

import java.util.ArrayList;

import jeremiahroque.com.weatherchecker.model.DailyWeatherForecast;
import jeremiahroque.com.weatherchecker.ui.AboutDialog;
import jeremiahroque.com.weatherchecker.ui.ForecastListAdapter;

/**
 * Displays the forecast in a list.
 */
public class ForecastActivity extends ActionBarActivity {

    public static final String KEY_FORECAST_DATA = "forecastData";
    public static final String KEY_LOCATION = "location";

    private ArrayList<DailyWeatherForecast> forecastList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        forecastList = bundle.getParcelableArrayList(KEY_FORECAST_DATA);

        ForecastListAdapter adapter = new ForecastListAdapter(this, forecastList);

        setContentView(R.layout.activity_forecast);

        String location = bundle.getString(ForecastActivity.KEY_LOCATION);
        setTitle(getString(R.string.title_activity_forecast, location));

        ListView listView = (ListView) findViewById(R.id.fiveDayForecast);
        listView.setAdapter(adapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_forecast, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            new AboutDialog(this).display();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
