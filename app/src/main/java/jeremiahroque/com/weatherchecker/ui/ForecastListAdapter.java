package jeremiahroque.com.weatherchecker.ui;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import jeremiahroque.com.weatherchecker.R;
import jeremiahroque.com.weatherchecker.model.DailyWeatherForecast;
import jeremiahroque.com.weatherchecker.model.WeatherInfo;

/**
 * Populates the ListView for ForecastActivity
 */
public class ForecastListAdapter extends ArrayAdapter<DailyWeatherForecast> {

    private final Context context;
    private final List<DailyWeatherForecast> weatherInfoArrayList;

    public ForecastListAdapter(Context context, List<DailyWeatherForecast> objects) {
        super(context, R.layout.forecast_result, objects);
        this.context = context;
        this.weatherInfoArrayList = objects;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View rowView = inflater.inflate(R.layout.forecast_result, parent, false);

        DailyWeatherForecast dailyWeather = weatherInfoArrayList.get(position);
        WeatherInfo infoToDisplay = dailyWeather.getHourlyWeatherForecastList().get(3);

        // Populate data
        TextView dateView = (TextView) rowView.findViewById(R.id.forecastDayOfWeek);
        dateView.setText(dailyWeather.getDate());

        TextView descriptionView = (TextView) rowView.findViewById(R.id.forecastDescription);
        descriptionView.setText(infoToDisplay.getWeatherDescription());

        TextView maxTemp = (TextView) rowView.findViewById(R.id.forecastMaxTemp);
        maxTemp.setText(Integer.toString(dailyWeather.getMaxTempFahrenheit()));

        TextView minTemp = (TextView) rowView.findViewById(R.id.forecastMinTemp);
        minTemp.setText(Integer.toString(dailyWeather.getMinTempFahrenheit()));

        TextView precipitation = (TextView) rowView.findViewById(R.id.forecastPrecipitation);
        precipitation.setText(context.getString(
                R.string.label_estimated_precipitation_in, infoToDisplay.getPrecipitationInches()));

        ImageView imageView = (ImageView) rowView.findViewById(R.id.forecastIcon);
        new ImagePopulator(imageView).execute(infoToDisplay.getWeatherIconURL());

        return rowView;
    }
}
