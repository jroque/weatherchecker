package jeremiahroque.com.weatherchecker.ui;

import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.widget.ImageView;

import java.io.InputStream;
import java.net.URL;

/**
 * This class is required as network calls cannot be on the UI thread.
 * It downloads the image requested and sets the ImageView's drawable to the downloaded image.
 */
public class ImagePopulator extends AsyncTask<String, Void, Drawable> {

    private final ImageView imageView;

    public ImagePopulator(ImageView imageView) {
        this.imageView = imageView;
    }

    @Override
    protected Drawable doInBackground(String... url) {
        try {
            InputStream is = (InputStream) new URL(url[0]).getContent();
            Drawable d = Drawable.createFromStream(is, "src name");
            return d;
        } catch (Exception e) {
            return null;
        }
    }

    protected void onPostExecute(Drawable drawable) {
        imageView.setImageDrawable(drawable);
    }

}
