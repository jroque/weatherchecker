package jeremiahroque.com.weatherchecker.ui;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

import jeremiahroque.com.weatherchecker.R;

/**
 * Simple class to display the "About WeatherChecker" box.
 */
public class AboutDialog {

    private Context context;

    public AboutDialog(Context context) {
        this.context = context;
    }

    public void display() {
        AlertDialog myDialog = new AlertDialog.Builder(context).create();
        myDialog.setMessage(context.getString(R.string.about_text));
        myDialog.setButton(DialogInterface.BUTTON_POSITIVE, context.getString(R.string.button_positive), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        myDialog.show();
    }
}
