package jeremiahroque.com.weatherchecker;

import android.content.Context;
import android.content.Intent;
import android.graphics.Point;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Display;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.TranslateAnimation;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Map;

import jeremiahroque.com.weatherchecker.model.DailyWeatherForecast;
import jeremiahroque.com.weatherchecker.model.WeatherInfo;
import jeremiahroque.com.weatherchecker.ui.AboutDialog;
import jeremiahroque.com.weatherchecker.ui.ImagePopulator;
import jeremiahroque.com.weatherchecker.worker.WeatherFetcher;
import jeremiahroque.com.weatherchecker.worker.WeatherFetcherListener;

/**
 * Fetches (all) weather data & displays the current conditions.
 */
public class MainActivity extends ActionBarActivity implements View.OnKeyListener, WeatherFetcherListener {

    public static final String KEY_CURRENT_WEATHER = "currentWeatherData";
    public static final String KEY_DRAW_HINT = "drawHint";
    public static final String KEY_FORECAST = "forecastData";

    private Animation cardSlide;
    private ArrayList<DailyWeatherForecast> fiveDayForecastList;
    private LinearLayout activityContainer;
    private WeatherInfo currentWeather;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (savedInstanceState != null) {
            currentWeather = savedInstanceState.getParcelable(KEY_CURRENT_WEATHER);
            fiveDayForecastList = savedInstanceState.getParcelableArrayList(KEY_FORECAST);
        }

        setContentView(R.layout.activity_main);
        findViewById(R.id.locationInput).setOnKeyListener(this);

        activityContainer = (LinearLayout) findViewById(R.id.container);
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);

        cardSlide = new TranslateAnimation(0.0f, 0.0f, size.y, 0.0f);
        cardSlide.setInterpolator(new LinearInterpolator());
        cardSlide.setDuration(500);

        if (currentWeather != null) {
            drawCard(false);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            new AboutDialog(this).display();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Saving the state keeps the card from disappearing when device orientation changes
     * @param savedInstanceState
     */
    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        savedInstanceState.putParcelable(KEY_CURRENT_WEATHER, currentWeather);
        savedInstanceState.putParcelableArrayList(KEY_FORECAST, fiveDayForecastList);
        super.onSaveInstanceState(savedInstanceState);
    }

    /**
     * Reads input when search is pressed on the keyboard
     */
    @Override
    public boolean onKey(View v, int keyCode, KeyEvent event) {
        if (keyCode == EditorInfo.IME_ACTION_SEARCH ||
            keyCode == EditorInfo.IME_ACTION_DONE ||
            event.getAction() == KeyEvent.ACTION_DOWN &&
            event.getKeyCode() == KeyEvent.KEYCODE_ENTER) {

            String locationQuery = ((EditText) v).getText().toString();
            new WeatherFetcher(this, getString(R.string.api_key_premium)).execute(locationQuery);
            return true;
        }
        return false;
    }

    /**
     * Display the search results (or not)
     * @param weatherResult
     */
    @Override
    public void callback(Map<String, Object> weatherResult) {

        currentWeather = (WeatherInfo) weatherResult.get(WeatherFetcher.KEY_CURRENT_CONDITION);
        fiveDayForecastList = (ArrayList<DailyWeatherForecast>) weatherResult.get(WeatherFetcher.KEY_FORECAST);

        FrameLayout cardViewTest = (FrameLayout) findViewById(R.id.currentWeatherCard);
        if (cardViewTest != null) {
            activityContainer.removeView(cardViewTest);
        }

        if (currentWeather != null) {
            drawCard(true);
        } else {
            toast(R.string.search_error);
        }
    }

    public void toFiveDayForecast(View v) {
        Intent intent = new Intent(this, ForecastActivity.class);
        intent.putParcelableArrayListExtra(ForecastActivity.KEY_FORECAST_DATA, fiveDayForecastList);
        intent.putExtra(ForecastActivity.KEY_LOCATION, currentWeather.getLocation());
        startActivity(intent);
    }

    private void drawCard(boolean animate) {
        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        FrameLayout cardView = (FrameLayout) inflater.inflate(R.layout.current_weather_conditions, null);

        activityContainer.addView(cardView);
        if (animate) {
            // Animate only if the search is run, not on an orientation change
            cardView.startAnimation(cardSlide);
        }

        ImageView currentWeatherCard = (ImageView) findViewById(R.id.weatherImage);
        new ImagePopulator(currentWeatherCard).execute(currentWeather.getWeatherIconURL());

        TextView currentTemperature = (TextView) findViewById(R.id.weatherTemperature);
        currentTemperature.setText(getString(R.string.label_temp_f, currentWeather.getCurrentFahrenheit()));

        TextView weatherDescription = (TextView) findViewById(R.id.weatherDescription);
        weatherDescription.setText(currentWeather.getWeatherDescription());

        TextView precipitationChance = (TextView) findViewById(R.id.weatherPrecipitation);
        precipitationChance.setText(getString(R.string.label_precipitation_in, currentWeather.getPrecipitationInches()));

        TextView date = (TextView) findViewById(R.id.weatherDate);
        date.setText(currentWeather.getDate());
    }

    private void toast(int stringResId) {
        Toast t = Toast.makeText(this, stringResId, Toast.LENGTH_LONG);
        t.setGravity(Gravity.BOTTOM, 0, 100);
        t.show();
    }
}
