package jeremiahroque.com.weatherchecker.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Class representing a "current_condition" weather reading.
 * Parcelable so it can be passed from MainActivity to ForecastActivity.
 */
public class WeatherInfo implements Parcelable {

    public static final Parcelable.Creator<WeatherInfo> CREATOR
            = new Parcelable.Creator<WeatherInfo>() {
        public WeatherInfo createFromParcel(Parcel in) {
            return new WeatherInfo(in);
        }

        public WeatherInfo[] newArray(int size) {
            return new WeatherInfo[size];
        }
    };

    private int currentFahrenheit;
    private float precipitationInches;
    private String date;
    private String location;
    private String weatherDescription;
    private String weatherIconURL;

    public WeatherInfo() {}

    public WeatherInfo(Parcel in) {
        currentFahrenheit = in.readInt();
        precipitationInches = in.readFloat();
        date = in.readString();
        location = in.readString();
        weatherDescription = in.readString();
        weatherIconURL = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(currentFahrenheit);
        dest.writeFloat(precipitationInches);
        dest.writeString(date);
        dest.writeString(location);
        dest.writeString(weatherDescription);
        dest.writeString(weatherIconURL);
    }

    public int getCurrentFahrenheit() {
        return currentFahrenheit;
    }

    public void setCurrentFahrenheit(int currentFahrenheit) {
        this.currentFahrenheit = currentFahrenheit;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public float getPrecipitationInches() {
        return precipitationInches;
    }

    public void setPrecipitationInches(float precipitationInches) {
        this.precipitationInches = precipitationInches;
    }

    public String getWeatherDescription() {
        return weatherDescription;
    }

    public void setWeatherDescription(String weatherDescription) {
        this.weatherDescription = weatherDescription;
    }

    public String getWeatherIconURL() {
        return weatherIconURL;
    }

    public void setWeatherIconURL(String weatherIconURL) {
        this.weatherIconURL = weatherIconURL;
    }
}
