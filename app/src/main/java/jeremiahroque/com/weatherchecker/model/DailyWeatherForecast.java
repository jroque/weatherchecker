package jeremiahroque.com.weatherchecker.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Class representing the "weather" data point returned from worldweatheronline.
 * Parcelable so it can be passed from MainActivity to ForecastActivity
 */
public class DailyWeatherForecast implements Parcelable {

    public static final Parcelable.Creator<DailyWeatherForecast> CREATOR
            = new Parcelable.Creator<DailyWeatherForecast>() {
        public DailyWeatherForecast createFromParcel(Parcel in) {
            return new DailyWeatherForecast(in);
        }

        public DailyWeatherForecast[] newArray(int size) {
            return new DailyWeatherForecast[size];
        }
    };

    private ArrayList<WeatherInfo> hourlyWeatherForecastList;
    private String date;
    private int minTempFahrenheit;
    private int maxTempFahrenheit;

    public DailyWeatherForecast() {}

    private DailyWeatherForecast(Parcel in) {
        hourlyWeatherForecastList = in.readArrayList(getClass().getClassLoader());
        date = in.readString();
        maxTempFahrenheit = in.readInt();
        minTempFahrenheit = in.readInt();
    }

    /* Methods for data marshalling, for when sending using  an Intent */

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeList(hourlyWeatherForecastList);
        dest.writeString(date);
        dest.writeInt(maxTempFahrenheit);
        dest.writeInt(minTempFahrenheit);
    }

    public ArrayList<WeatherInfo> getHourlyWeatherForecastList() {
        return hourlyWeatherForecastList;
    }

    public void setHourlyWeatherForecastList(ArrayList<WeatherInfo> hourlyWeatherForecastList) {
        this.hourlyWeatherForecastList = hourlyWeatherForecastList;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getMinTempFahrenheit() {
        return minTempFahrenheit;
    }

    public void setMinTempFahrenheit(int minTempFahrenheit) {
        this.minTempFahrenheit = minTempFahrenheit;
    }

    public int getMaxTempFahrenheit() {
        return maxTempFahrenheit;
    }

    public void setMaxTempFahrenheit(int maxTempFahrenheit) {
        this.maxTempFahrenheit = maxTempFahrenheit;
    }
}